// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}

const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName : () => {
        console.log(firstName + " " + lastName)
      }
    }
  }

 
//Driver Code 
newFunction("William", "Imoh").fullName() 
