// Diberikan sebuah objek sebagai berikut:
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
//const firstName = newObject.firstName;
//const lastName = newObject.lastName;
//const address = newObject.address;
//const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat 
// (1 line saja)
const {firstName,lastName,address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)
