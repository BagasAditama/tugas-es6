// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
let after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet} `

console.log(before)
console.log(before)